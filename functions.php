<?php



function setup_post_types_reidis() {

    add_theme_support( 'post-thumbnails', array( 'post', 'gallery' ) );

    $args = array(
        'public'    => true,
        'label'     => __( 'Gallery', 'textdomain' ),
        'menu_icon' => 'dashicons-book',
        'supports' => array( 'thumbnail' )
    );
    register_post_type( 'gallery', $args );

    register_nav_menus(
        array(
        'primary-menu' => __( 'Primary Menu' ),
        )
    );
    
}
add_action( 'init', 'setup_post_types_reidis' );
