<?php
/**
* Template Name: About us
*/

get_header() ?>

		<section
			class="pageBanner"
			style="
				background-image: url('<?php echo get_template_directory_uri() ?>/img/contact-hero.jpg');
				background-size: cover;
			"
		>
			<div class="pageBannerText">
				<h1>Hello! We are <span class="dpx-color">Decopex</span> Interiors.</h1>
				<p>
					Established on 2013, the company continues to serve with great pride
					and honor,<br />
					having more than a thousand satisfied clients.
				</p>
			</div>
		</section>
		<section class="pageSection about-us-page">
			<div class="container">
				<div class="row">
					<div class="about-us-section">
						<div>
							<img
								src="<?php echo get_template_directory_uri() ?>/img/our_first_brick.jpg"
								alt=""
								class="img-responsive"
							/>
						</div>
						<div class="about-us-text">
							<h1 class="fw-300">Our First Brick</h1>
							<p>
								We laid our first brick a decade ago, founding Decopex in the
								heart of Dubai. Since then, we have evolved to become one of the
								leading interior fit-out firms led by artisan brains providing
								custom design and turnkey project management. We believe in
								creating exceptional spaces that emanate the brand from its
								heart, working effortlessly to offer solutions that set the
								benchmark high.
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="about-us-section justify-reverse">
						<div>
							<img
								src="<?php echo get_template_directory_uri() ?>/img/laying_building_blocks.jpg"
								alt=""
								class="img-responsive"
							/>
						</div>
						<div class="about-us-text">
							<h1 class="fw-300">Laying Building Blocks</h1>
							<p>
								Spaces must espouse welfare and articulate values of the labels
								they house. From rustic beauty to eclectic style, our mission is
								to creatively solve the challenge of balancing elegance,
								comfort, functionality and sustainability by giving a unique
								visual personality to corporate, retail, homes, offices and
								leisure facilities.
							</p>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="about-us-section">
						<div>
							<img src="<?php echo get_template_directory_uri() ?>/img/our_reputation.jpg" alt="" class="img-responsive" />
						</div>
						<div class="about-us-text">
							<h1 class="fw-300">Our Reputation Is Over The Roof!</h1>
							<p>
								We pride ourselves on continuing to push the boundaries of
								design, learn better techniques, and work harder in each
								project. Beyond beautiful conceptual drawings, our technical
								expertise blends form and function to ensure that every single
								detail is constructed with precision. That is precisely what
								makes us the preferred interior fit-out firm by our clients and
								enables us to create finely crafted spaces with a signature
								style.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="vspace-50"></div>

		<section class="pageSection about-us-page">
			<div class="container">
				<div class="row">
					<div class="about-us-section">
						<h1>Our Work <span class="blue">Structure</span></h1>
					</div>
				</div>
				<div class="row grey">
					<div class="about-us-section reverse-column">
						<div class="about-us-text">
							<p>
								At Decopex, we promise to bring your imagination alive.
								Conception is the beginning of the creative process. We develop
								architectural layouts for your property while investigating
								finishes, design styles and influences, then create renders and
								early 3D models describing the volumetric aspects of the design.
								The concept will also include refinement of the internal layouts
								and specify key finishes and materials for the floors, walls,
								furniture etc. We constantly challenge ourselves by reimagining
								how a particular space can enrich our clients’ experience.
							</p>
						</div>
						<div class="center">
							<div>
								<h1 class="d-inline">Sketch</h1>
								<img src="./<?php echo get_template_directory_uri() ?>/img/sketch.svg" alt="" class="d-inline" />
							</div>
							<h3>Idea & Concept</h3>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="about-us-section reverse-column">
						<div class="about-us-text">
							<p>
								Throughout the design stage, we get into more details. With each
								step, you will see the ideas coming to life as we establish the
								overall creative direction. At this stage, we decide with you
								the scheme, material and finishes that will be used; and a
								coordinated, fully detailed, technical construction plan,
								including breakups on refurbs and all the proposed new details
								for the build. The Decopex team will take you through the plan
								and communicate and clarify the design intent of each process.
							</p>
						</div>
						<div class="center">
							<div>
								<h1 class="d-inline">Create</h1>
								<img src="./<?php echo get_template_directory_uri() ?>/img/create.svg" alt="" class="d-inline" />
							</div>
							<h3>Design & Curate</h3>
						</div>
					</div>
				</div>
				<div class="row blue">
					<div class="about-us-section reverse-column">
						<div class="about-us-text">
							<p>
								During the project execution, we will work closely with the
								contractors and utility supply chain to successfully implement
								our design, install, and commission all the associated fixtures
								and equipment. We ensure that your chosen materials compliment
								the room and all fixed elements work together to present the
								most realistic version of your imagination.
							</p>
						</div>
						<div class="center">
							<div>
								<h1 class="d-inline">Build</h1>
								<img src="./<?php echo get_template_directory_uri() ?>/img/build.png" alt="" class="d-inline" />
							</div>
							<h3>Construct & Install</h3>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="vspace-50"></div>

		<footer class="text-center">
			<p>© 2021 Decopex Interiors All Rights Reserved.</p>
		</footer>

<?php get_footer() ?>
