<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
  
get_header(); ?>





<section
			class="pageBanner"
			style="
				background-image: url('<?php echo get_template_directory_uri() ?>/img/hero-services.jpg');
				background-size: cover;
			"
		>
			<div class="pageBannerText">
				<h1><span class="blue">Services</span> we provide.</h1>
			</div>
		</section>

		<section class="homeSection timeline" id="timeline">
			<div class="container">
				<!-- <div class="section-title text-center">
					 <h2><span class="reveal-text">HOW IT WORKS</span></h2>
				</div> -->
                <?php
                    // Start the loop.
                    while ( have_posts() ) : the_post();
            
                        /*
                        * Include the post format-specific template for the content. If you want to
                        * use this in a child theme, then include a file called called content-___.php
                        * (where ___ is the post format) and that will be used instead.
                        */
                        the_post()
            
                    endwhile;
                    ?>
			</div>
		</section>

  
<?php get_footer(); ?>