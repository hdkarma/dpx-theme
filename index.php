<?php get_header() ?>
<section class="homeSection" id="banner">
			<!-- <div class="bannerText two">
				<div class="bannerSliderWrap">
					<div class="bannerSlider">
						<div class="bannerSlide">
							<h1>
								<span class="light"
									>Handcraft <br />your
									<span class="blue">Dreams.</span>
								</span>
							</h1>
						</div>
					</div>
				</div>
			</div> -->

			<h1>
				<span class="light">
					Handcraft <br />
					your
					<span class="blue">Dreams.</span>
				</span>
			</h1>

			<div class="homepage-hero-module">
				<div class="video-container">
					<video
						autoplay="autoplay"
						muted="muted"
						loop="loop"
						class="fillWidth"
					>
						<source
							src="https://res.cloudinary.com/reidis-interactive/video/upload/v1630572575/samples/reidishost/Dpx_ziqbhv.mp4"
							type="video/mp4"
						/>

						<source src="./Dpx.mp4" type="video/mp4" />
						Your browser does not support the video tag. I suggest you upgrade
						your browser.
						<source src="video/interior.ogg" type="video/ogg" />
						Your browser does not support the video tag. I suggest you upgrade
						your browser.
						<source src="./video/Dpx.webm" type="video/webm" />
						Your browser does not support the video tag. I suggest you upgrade
						your browser.
					</video>

					<!-- <video
						src="https://res.cloudinary.com/reidis-interactive/video/upload/v1630572575/samples/reidishost/Dpx_ziqbhv.mp4"
						autoplay
						muted
						loop
					></video> -->
					<!-- <video src="./Dpx.mp4" autoplay muted loop></video> -->
				</div>
				<div id="cf">
					<div
						class="cf-img"
						style="background-image: url('./img/works/1624956233891.jpg')"
					></div>
					<div
						class="cf-img"
						style="background-image: url('/img/works/1624956233916.jpg')"
					></div>
					<div
						class="cf-img"
						style="background-image: url('/img/works/AD6I9631.jpg')"
					></div>
				</div>
			</div>
		</section>
<?php get_footer() ?>
                            