$(document).ready(function () {
	$('.gallery-img').click(function () {
		var t = $(this).attr('src');
		$('.modal-body').html("<img src='" + t + "' class='modal-img'>");
		$('#myModal').modal();
	});
	var prevScrollpos = window.pageYOffset;
	window.onscroll = function () {
		var currentScrollPos = window.pageYOffset;
		if (prevScrollpos > currentScrollPos) {
			$('.navbar-brand img').css('width', '60px');
		} else {
			$('.navbar-brand img').css('width', '40px !important');
		}
		prevScrollpos = currentScrollPos;
	};
});
