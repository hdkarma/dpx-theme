//jQuery is required to run this code
$(document).ready(function () {
	scaleVideoContainer();

	initBannerVideoSize('.video-container .poster img');
	initBannerVideoSize('.video-container .filter');
	initBannerVideoSize('.video-container video');

	$(window).on('resize', function () {
		scaleVideoContainer();
		scaleBannerVideoSize('.video-container .poster img');
		scaleBannerVideoSize('.video-container .filter');
		scaleBannerVideoSize('.video-container video');
	});

	$('.bannerSlider').slick({
		dots: false,
		arrows: false,
		infinite: true,
		speed: 500,
		autoplay: true,
		fade: true,
		cssEase: 'linear',
	});

	$('nav .close').click(function () {
		if ($('body').hasClass('menuOpen')) {
			$('body').removeClass('menuOpen');
		}
	});

	$('.menu').click(function () {
		console.log('sd');
		$('body').hasClass('menuOpen')
			? $('body').removeClass('menuOpen')
			: $('body').addClass('menuOpen');
	});

	// -----------------------------------------------
	// Scroll to top
	// -----------------------------------------------

	$(window).scroll(function () {
		var y = $(this).scrollTop();
		if (y > 800) {
			$('.scroll-top').fadeIn();
		} else {
			$('.scroll-top').fadeOut();
		}
	});

	$('.scroll-top').click(function () {
		window.scroll({
			top: 0,
			behavior: 'smooth',
		});
	});
});

function scaleVideoContainer() {
	var height = $(window).height() + 5;
	var unitHeight = parseInt(height) + 'px';
	$('.homepage-hero-module').css('height', unitHeight);
}

function initBannerVideoSize(element) {
	$(element).each(function () {
		$(this).data('height', $(this).height());
		$(this).data('width', $(this).width());
	});

	scaleBannerVideoSize(element);
}

function scaleBannerVideoSize(element) {
	var windowWidth = $(window).width(),
		windowHeight = $(window).height() + 5,
		videoWidth,
		videoHeight;

	// console.log(windowHeight);

	$(element).each(function () {
		var videoAspectRatio = $(this).data('height') / $(this).data('width');

		$(this).width(windowWidth);

		if (windowWidth < 1000) {
			videoHeight = windowHeight;
			videoWidth = videoHeight / videoAspectRatio;
			$(this).css({
				'margin-top': 0,
				'margin-left': -(videoWidth - windowWidth) / 2 + 'px',
			});

			$(this).width(videoWidth).height(videoHeight);
		}

		$('.homepage-hero-module .video-container video').addClass(
			'fadeIn animated'
		);
	});
}
