
		<div class="side-click">
			<a href="https://wa.me/971524292682" class="chat">
				<span class="fa-stack fa-2x" style="vertical-align: top">
					<i class="fas fa-circle fa-stack-2x"></i>
					<i class="fab fa-whatsapp fa-stack-1x fa-inverse"></i>
				</span>
			</a>
		</div>

		<script src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery-1.11.2.min.js"></script>

		<script src="<?php echo get_template_directory_uri() ?>/js/vendor/bootstrap.min.js"></script>
		<script src="<?php echo get_template_directory_uri() ?>/js/vendor/slick.min.js"></script>
		<script src="https://dixonandmoe.com/assets/javascripts/rellax.min-165faf51.js"></script>
		<script src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery.waypoints.min.js"></script>
		<script
			type="text/javascript"
			src="https://cdnjs.cloudflare.com/ajax/libs/SocialIcons/1.0.1/soc.min.js"
		></script>
		<script src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery.counterup.min.js"></script>
		<script src="<?php echo get_template_directory_uri() ?>/js/vendor/jquery.viewportchecker.min.js"></script>
		<script src="<?php echo get_template_directory_uri() ?>/js/main.js"></script>
		<script src="https://unpkg.com/aos@next/dist/aos.js"></script>
		<!-- <script>
		</script> -->

		<script>
			// const vid = document.querySelector('video');
			// vid.play();
			$(document).ready(function () {
				//$('.bannerSlider').slick();
				AOS.init();

				var rellax = new Rellax('.rellax', {
					center: true,
					round: true,
					vertical: true,
					horizontal: false,
				});

				$('.scrollDown .ico').click(function () {
					var body = $('html, body');
					var posY = $('#intro').offset().top - $('.navbar').height();
					body.stop().animate(
						{
							scrollTop: posY,
						},
						500,
						'swing',
						function () {}
					);
				});

				$('.homeSection').viewportChecker();
				$('#navbar .navbar-toggle').click(function (e) {
					$(this).toggleClass('mOpen');
				});

				$('.counter').counterUp({
					delay: 10,
					time: 5000,
				});

				$('.testimonialSlider').slick({
					infinite: true,
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
					autoplay: true,
				});

				$('.partners').slick({
					infinite: true,
					slidesToShow: 3,
					slidesToScroll: 1,
					arrows: false,
					dots: true,
					autoplay: true,
				});
			});

			$(window).scroll(function (e) {
				if ($(window).scrollTop() > 0) {
					$('body').addClass('bodyScrolled');
				} else {
					$('body').removeClass('bodyScrolled');
				}
			});

			// Amigos-Service

			$('.service-nav li:first').addClass('active');
			$('.service-content').hide();
			$('.service-content:first').show();

			$('.service-nav li').click(function () {
				var i = $(this).attr('id');

				// setting the clicked option as active
				if (!$(this).hasClass('active')) {
					$('.service-nav li').removeClass('active');
					$(this).addClass('active');
				}

				$('.service-content').hide();
				$('#' + i + 'C').fadeIn('slow');
			});
			var tag = document.createElement('script');

			tag.src = 'https://www.youtube.com/iframe_api';
			var firstScriptTag = document.getElementsByTagName('script')[0];
			firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

			var isiOS = navigator.userAgent.match(/(iPad)|(iPhone)|(iPod)/i) != null; //boolean check for iOS devices

			var youtubelightbox = document.getElementById('youtubelightbox');
			var player; // variable to hold new YT.Player() instance

			// Hide lightbox when clicked on
			youtubelightbox && youtubelightbox.addEventListener(
				'click',
				function () {
					this.style.display = 'none';
					player.stopVideo();
				},
				false
			);

			// Exclude youtube iframe from above action
			youtubelightbox && youtubelightbox.querySelector('.centeredchild').addEventListener(
				'click',
				function (e) {
					e.stopPropagation();
				},
				false
			);

			// define onYouTubeIframeAPIReady() function and initialize lightbox when API is ready
			function onYouTubeIframeAPIReady() {
				createlightbox();
			}

			// Extracts the Youtube video ID from a well formed Youtube URL
			function getyoutubeid(link) {
				// Assumed Youtube URL formats
				// https://www.youtube.com/watch?v=Pe0jFDPHkzo
				// https://youtu.be/Pe0jFDPHkzo
				// https://www.youtube.com/v/Pe0jFDPHkzo
				// and more

				//See http://stackoverflow.com/a/6904504/4360074
				var youtubeidreg =
					/(?:youtube\.com\/(?:[^\/]+\/.+\/|(?:v|e(?:mbed)?)\/|.*[?&]v=)|youtu\.be\/)([^"&?\/ ]{11})/i;
				return youtubeidreg.exec(link)[1]; // return Youtube video ID portion of link
			}

			// Creates a new YT.Player() instance
			function createyoutubeplayer(videourl) {
				player = new YT.Player('playerdiv', {
					videoId: videourl,
					playerVars: {
						autoplay: 1,
					},
				});
			}

			// Main Youtube lightbox function
			function createlightbox() {
				var targetlinks = document.querySelectorAll('.lightbox');
				for (var i = 0; i < targetlinks.length; i++) {
					var link = targetlinks[i];
					link._videoid = getyoutubeid(link); // store youtube video ID portion of link inside _videoid property
					targetlinks[i].addEventListener(
						'click',
						function (e) {
							youtubelightbox.style.display = 'block';
							if (typeof player == 'undefined') {
								// if video player hasn't been created yet
								createyoutubeplayer(this._videoid);
							} else {
								if (isiOS) {
									// iOS devices can only use the "cue" related methods
									player.cueVideoById(this._videoid);
								} else {
									player.loadVideoById(this._videoid);
								}
							}
							e.preventDefault();
						},
						false
					);
				}
			}
		</script>
    <?php wp_footer();?>

	</body>
</html>
