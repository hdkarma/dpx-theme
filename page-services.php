<?php
/**
* Template Name: Services
*/

get_header() ?>
		<section
			class="pageBanner"
			style="
				background-image: url('<?php echo get_template_directory_uri() ?>/img/hero-services.jpg');
				background-size: cover;
			"
		>
			<div class="pageBannerText">
				<h1><span class="blue">Services</span> we provide.</h1>
			</div>
		</section>

		<section class="homeSection timeline" id="timeline">
			<div class="container">
				<!-- <div class="section-title text-center">
					 <h2><span class="reveal-text">HOW IT WORKS</span></h2>
				</div> -->
				<div class="timeline-wrapper">
					<div class="timeline-block">
						<div
							class="timeline-img"
							style="background-image: url('<?php echo get_template_directory_uri() ?>/img/img_03.jpg')"
						></div>
						<div class="timeline-content">
							<div class="line-top"></div>
							<div class="line-right"></div>
							<div class="line-bottom"></div>
							<div class="line-left"></div>
							<h3 class="blue">DESIGNING & APPROVAL</h3>
							<p>
								With the experience and expertise of our company for over the
								years, our experts stretch consultation for your interior design
								ideas. We make designs and projects according to the
								specifications, requirements, and budget of our clients. We
								listen and talk to the needs and wants of our clients based on
								the consultation of municipality approval, interior fit-out, 2D
								designing and 3D visualization.
							</p>
						</div>
					</div>
					<div class="timeline-block reverse">
						<div
							class="timeline-img"
							style="background-image: url('<?php echo get_template_directory_uri() ?>/img/img_02.jpg')"
						></div>
						<div class="timeline-content">
							<div class="line-top"></div>
							<div class="line-right"></div>
							<div class="line-bottom"></div>
							<div class="line-left"></div>
							<h3 class="blue">FIT-OUT CONTRACTING</h3>
							<p>
								The process of crafting and editing technical drawings and
								creating designs, is 2D drafting. Our company develops floor
								plans, building permit drawings, building inspection plans and
								landscaping layouts. Using advance computer-aided design
								software, our expert artists draft designs and drawings more
								quickly and accurately, highlighting the precision with stencils
								and technical drawing instruments.
							</p>
						</div>
					</div>
					<div class="timeline-block">
						<div
							class="timeline-img"
							style="background-image: url('<?php echo get_template_directory_uri() ?>/img/img_04.jpg')"
						></div>
						<div class="timeline-content">
							<div class="line-top"></div>
							<div class="line-right"></div>
							<div class="line-bottom"></div>
							<div class="line-left"></div>
							<h3 class="blue">MAINTENANCE</h3>
							<p>
								Apart from fit-out designs and refurbishment services of the
								Crystal Bright Interior, our company provides professional
								solutions for Interior fit-out – business offices, restaurants,
								warehouses and among others. Ensuring the easy and fast fit-out
								approval, we – our company – process it without any kinds of
								hassle in timely and rightly manner.
							</p>
						</div>
					</div>
				</div>
			</div>
		</section>

		<footer class="text-center">
			<p>© 2021 Decopex Interiors All Rights Reserved.</p>
		</footer>
		<?php get_footer() ?>
