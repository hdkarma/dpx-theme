<!DOCTYPE html>
<html class="no-js" lang="">
	<head>
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<title>Decopex Interiors</title>
		<meta name="description" content="" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<link rel="apple-touch-icon" href="img/favicon.png" />
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png" />
		<link
			href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700"
			rel="stylesheet"
		/>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/slick.css" />

		<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/css/inner-style.css" />


		<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />
		<style>
			body {
				margin: 0;
				margin: 0;
				scrollbar-width: none;
			}
		</style>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/main.css" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/developer.css" />
		<link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/css/youtube.css" />
		<script
			src="https://kit.fontawesome.com/9c183d1175.js"
			crossorigin="anonymous"
		></script>
		<link
			rel="stylesheet"
			type="text/css"
			href="https://cdnjs.cloudflare.com/ajax/libs/SocialIcons/1.0.1/soc.min.css"
		/>
		<!-- <script type="text/javascript">
			window.addEventListener('beforeunload', function (event) {
				window.scrollTo(0, 0);
			});
		</script> -->
		<script src="<?php echo get_template_directory_uri() ?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <?php wp_head();?>
	</head>

	<body>
		<div
			class="soc"
			data-size="40px"
			data-buttoncolor="#f5f5f5"
			data-iconcolor="#1ab0da"
			data-radius="0"
		>
			<a href="#" target="_blank" class="soc-facebook" title="Facebook"></a>
			<a href="#" target="_blank" class="soc-instagram" title="Instagram"></a>
			<a href="#" target="_blank" class="soc-linkedin" title="linkedin"></a>
		</div>

		<header>
			<nav class="navbar navbar-default navbar-fixed-top">
				<div class="container-fluid">
					<div class="navbar-header">
						<button
							type="button"
							class="navbar-toggle collapsed"
							data-toggle="collapse"
							data-target="#navbar"
							aria-expanded="false"
							aria-controls="navbar"
						>
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.html" class="logo"
							><img src="img/logo.png" alt=""
						/></a>
					</div>
					<div id="navbar" class="navbar-collapse collapse">
						<button type="button" class="navbar-toggle mOpen">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>

						
						<?php 
							wp_nav_menu( array(
								'menu'           => 'primary-menu', // Do not fall back to first non-empty menu.
								'menu_class' => 'nav navbar-nav navbar-right',     
								'theme_location' => 'primary-menu',
								'container' => ''
							) );
							?>
					</div>
				</div>
			</nav>
		</header>

