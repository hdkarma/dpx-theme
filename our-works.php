<?php
/**
* Template Name: Our Works
*/

get_header() ?>

		<section class="pageBanner" style="background-image: url('img/banner.jpg')">
			<div class="pageBannerText">
				<h1>Handcrafted <span class="blue">Dreams.</span></h1>
				<!-- <a href="our-works.html" class="white">
					<p>Our Projects <span class="orange">&#10230;</span></p>
				</a> -->
			</div>
		</section>

		<section class="pageSection">
			<div class="container-fluid">
				<div class="gallery">

				<?php
				$args = array(  
					'post_type' => 'gallery',
					'post_status' => 'publish',
					'posts_per_page' => 8, 
				);
			
				$loop = new WP_Query( $args ); 
					
				while ( $loop->have_posts() ) : $loop->the_post(); 
					?>

					<img
						src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full') ?>"
						alt=""
						width="100%"
						height="auto"
						class="gallery-img"
					/>

					<?php
				endwhile;
			
				wp_reset_postdata(); 

				?>
				</div>
			</div>

			<div id="myModal" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body"></div>
					</div>
				</div>
			</div>
		</section>

		<div class="vspace-50"></div>
		<footer class="text-center">
			<p>© 2021 Decopex Interiors All Rights Reserved.</p>
		</footer>
		<?php get_footer() ?>
