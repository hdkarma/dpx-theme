<?php
/**
* Template Name: Contact
*/

get_header() ?>

		<section
			class="pageBanner"
			style="
				background-image: url('<?php echo get_template_directory_uri() ?>/img/contact-hero.jpg');
				background-size: cover;
			"
		>
			<div class="pageBannerText">
				<h1>Handcraft you <span class="blue">dreams.</span></h1>
				<p>
					It is our timeless promise to our customers to continue crafting and
					constructing opulent, original unvrying spaces.
				</p>
			</div>
		</section>

		<section class="pageSection" style="min-height: 35vh">
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<div class="contact-item text-center">
							<i class="fas fa-map-marker-alt orange"></i>
							<div class="contact-info">
								<h4 class="dark-color">Come And Meet Us</h4>
								<p>
									Al Qusais Industrial Area 3,<br />
									Dubai - UAE
								</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="contact-item text-center">
							<i class="fas fa-phone-alt orange"></i>
							<div class="contact-info">
								<h4 class="dark-color">Give Us A Call</h4>
								<p>+971 52 429 2682</p>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="contact-item text-center">
							<i class="fas fa-envelope orange"></i>
							<div class="contact-info">
								<h4 class="dark-color">Send Us A Message</h4>
								<p>
									For Information:
									<a href="mailto:info@thedecopex.com">info@thedecopex.com</a>
								</p>
								<p>
									For Enquiry:
									<a href="mailto:sales@thedecopex.com">sales@thedecopex.com</a>
								</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div class="mapbar">
			<iframe
				src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14428.769885272897!2d55.37838942288684!3d25.29773669156325!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f5c1a0fef5b31%3A0xf5506c3024b432e3!2sAl%20Qusais%20Industrial%20AreaAl%20Qusais%20Industrial%20Area%203%20-%20Dubai!5e0!3m2!1sen!2sae!4v1630497675917!5m2!1sen!2sae"
				width="100%"
				height="600"
				style="border: 0"
				allowfullscreen=""
				loading="lazy"
			></iframe>
		</div>

		<footer class="text-center">
			<p>© 2021 Decopex Interiors All Rights Reserved.</p>
		</footer>
		<?php echo get_footer() ?>
